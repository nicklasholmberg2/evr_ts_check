#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <string.h> // Provides memcpy prototype
#include <stdlib.h> // Provides calloc prototype
#include <stdio.h>
#include <math.h>


static int evr_ts_check(aSubRecord *precord) {
    int cntrangelast = 1;
    int cntzerolast = 1;
    int cntrangefirst = 1;
    int cntzerofirst = 1;
    


    //elmt = *(ulong *)precord->nea;
    //all timestamps copied to array
    int in_ts[1000];
    memcpy(in_ts, (int *)precord->a,  1000 * sizeof(int));
    int firstidxlow     = (*(int *)precord->b);
    int firstidxhigh    = (*(int *)precord->c);
    int firstidx        = (*(int *)precord->d);
    int lastidxlow      = (*(int *)precord->e);
    int lastidxhigh     = (*(int *)precord->f);
    int lastidx         = (*(int *)precord->g);


    if (firstidxlow < in_ts[firstidx] && in_ts[firstidx] < firstidxhigh){
        cntrangefirst = 0;
    }

    if (in_ts[firstidx] != 0){
        cntzerofirst = 0;
    }

    if (lastidxlow < in_ts[lastidx] && in_ts[lastidx] < lastidxhigh){
        cntrangelast = 0;
    }

    if (in_ts[lastidx] != 0){
        cntzerolast = 0;
    }
    
    printf("First idx: %d, second idx: %d, cntrnfirst: %d, cnt0first: %d, cntrnlast: %d, cnt0last: %d\n",in_ts[firstidx], in_ts[lastidx], cntrangefirst, cntzerofirst, cntrangelast, cntzerolast);

    *(int *)precord->vala = cntrangefirst;
    *(int *)precord->valb = cntzerofirst;
    *(int *)precord->valc = cntrangelast;
    *(int *)precord->vald = cntzerolast;
    


return 0;
}

epicsRegisterFunction(evr_ts_check);

